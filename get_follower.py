#!/usr/bin/python
import sys
from selenium import webdriver


def scraping_followers(twitter_url):
    xpath_follower_query = "//span[@class='css-901oao css-16my406 r-18jsvk2 r-poiln3 r-b88u0q r-bcqeeo r-qvutc0']"
    driver = webdriver.Firefox(executable_path='bin/geckodriver')
    driver.get(twitter_url)
    time_to_wait_in_second = 5
    driver.implicitly_wait(time_to_wait_in_second)
    follower_elems = driver.find_elements_by_xpath(xpath_follower_query)
    twitter_follower = follower_elems[1].text
    driver.close()
    return twitter_follower


if __name__ == '__main__':
    try:
        follower = scraping_followers(sys.argv[1])
        print("{} has {} followers".format(sys.argv[1], follower))
    except:
        print("Unable to get follower, please make sure you have an internet and input is valid twitter url")
